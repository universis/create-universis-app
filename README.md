![Universis](src/assets/img/universis_logo_128_color.png)

## Universis application template

A starter client application of [Universis project](https://gitlab.com/universis) written in Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production server

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/universis-app` directory.
